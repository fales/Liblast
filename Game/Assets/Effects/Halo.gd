#@tool
extends MeshInstance3D

@export var halo_size := 1.0:
	set(value):
		mesh.size = Vector2(value, value)
		halo_size = value
		
@onready var halo_transform = global_transform
@export var halo_color := Color.WHITE:
	set(value):
		get_surface_override_material(0)["albedo_color"] = value
		halo_color = value

var ray_previously := false

func _ready() -> void:
#	if Engine.is_editor_hint():
#		return
	mesh = mesh.duplicate()
	var halo_material = get_surface_override_material(0).duplicate()
	set_surface_override_material(0, halo_material)

func _process(delta: float) -> void:
#	if Engine.is_editor_hint():
#		return
	var space_state = get_world_3d().direct_space_state
	var physics_ray_query_parameters_3d = PhysicsRayQueryParameters3D.new()
	physics_ray_query_parameters_3d.from = global_transform.origin
	physics_ray_query_parameters_3d.to = get_viewport().get_camera_3d().global_transform.origin
	physics_ray_query_parameters_3d.exclude = [get_viewport().get_camera_3d().get_parent().get_parent()]
	var ray = space_state.intersect_ray(physics_ray_query_parameters_3d)
	
	#print(ray)
	
	if ray.size() > 0 and not ray_previously:
		hide()
	elif not ray.size() > 0 and ray_previously:
		show()
		
	if ray.size() > 0:
		ray_previously = true
	else:
		ray_previously = false
	
	if visible:
		var fade = 1 - clamp(pow(physics_ray_query_parameters_3d.from.distance_to(physics_ray_query_parameters_3d.to), 1.5) / 800, 0, 1)
		#print(fade)
		get_surface_override_material(0)["albedo_color"] = halo_color * fade
